package com.delta.bookings.services;

import java.util.List;

import com.delta.bookings.models.Booking;
import com.delta.bookings.repositories.BookingRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookingServiceImpl implements BookingService{

    @Autowired
    BookingRepo bookingRepo;

    @Override
    public List<Booking> findAll() {
        return bookingRepo.findAll();
    }

    @Override
    public Booking save(Booking booking) {
        bookingRepo.save(booking);
        return booking;
    }

    @Override
    public Booking findById(Long id) {
        if(bookingRepo.findById(id).isPresent()) {
            return bookingRepo.findById(id).get();
        }
        return null;
    }

    @Override
    public void delete(Long id) {
        Booking bookingToDelete = findById(id);
        bookingRepo.delete(bookingToDelete);        
    }
    
} // end of class

