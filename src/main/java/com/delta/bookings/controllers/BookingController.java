package com.delta.bookings.controllers;

import java.util.List;

import com.delta.bookings.models.Booking;
import com.delta.bookings.services.BookingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1")
public class BookingController {

    @Autowired
    BookingService sService;

    //Not needed for JWT auth:

    // @GetMapping(produces = "application/json")
	// @RequestMapping({ "/bookings/validateLogin" })
	// public User validateLogin() {
	// 	return new User("User successfully authenticated");
	// }

    @GetMapping("/bookings")
    public ResponseEntity<List<Booking>> get() {
        List<Booking> bookings = sService.findAll();
        return new ResponseEntity<List<Booking>>(bookings, HttpStatus.OK);
    }

    @PostMapping("/bookings")
    public ResponseEntity<Booking> save(@RequestBody Booking booking) {
        Booking newBooking = sService.save(booking);
        return new ResponseEntity<Booking>(newBooking, HttpStatus.OK);
    }
    
    @GetMapping("/bookings/{id}")
    public ResponseEntity<Booking> getBooking(@PathVariable("id") Long id) {
        Booking viewBooking = sService.findById(id);
        return new ResponseEntity<Booking>(viewBooking, HttpStatus.OK);
    }

    @DeleteMapping("/bookings/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        sService.delete(id);
        return new ResponseEntity<String>("Booking was deleted successfully.", HttpStatus.OK);
    }
            
} // end of class

