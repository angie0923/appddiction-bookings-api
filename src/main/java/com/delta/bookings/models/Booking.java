package com.delta.bookings.models;

// import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "bookings")
@Getter
@Setter
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String origEntryDate;

    private String guestFirstName;

    private String guestLastName;

    private String guestEmail;

    private String guestCellphone;

    private Integer partyOf;
    
    private String locationRequested;

    private String arrivalDate;

    private String departureDate;

    private String status;

    private String lastModifiedDate;

    private String reviewedBy;

    private String lastReviewDate;

}
